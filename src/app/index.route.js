(function() {
  'use strict';

  angular
    .module('angularGulp', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngMessages',
      'ngAria',
      'ngResource',
      'ngRoute',
      'ui.bootstrap',
      'toastr'])
    .config(function($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'app/main/main.html',
          controller: 'MainController',
          controllerAs: 'main'
        })
        .when('/nextP', {
          templateUrl: 'app/main/template/nextPages/list.html',
          controller: 'NextController',
          controllerAs: 'main'
        })
        .otherwise({
          redirectTo: '/'
        });
    });




})();
